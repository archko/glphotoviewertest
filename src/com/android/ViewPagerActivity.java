/**
 * ****************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *****************************************************************************
 */
package com.android;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import cn.archko.gallery3d.R;
import com.android.launcher3.GLImageView;
import com.android.photos.BitmapRegionTileSource;
import com.android.photos.views.TiledImageView;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.List;

/**
 * Lock/Unlock button is added to the ActionBar.
 * Use it to temporarily disable ViewPager navigation in order to correctly interact with ImageView by gestures.
 * Lock/Unlock state of ViewPager is saved and restored on configuration changes.
 * <p/>
 * Julia Zudikova
 */
public class ViewPagerActivity extends Activity {

    private static final String ISLOCKED_ARG = "isLocked";

    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
        mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
        setContentView(mViewPager);

        loadData();
    }

    void loadData() {
        new AsyncTask<Object, Object, List<File>>() {
            @Override
            protected List<File> doInBackground(Object... params) {
                File dir = new File("/sdcard/.microblog/picture/");
                if (dir.exists()) {
                    File[] files = dir.listFiles(new FileFilter() {
                        @Override
                        public boolean accept(File pathname) {
                            int i = 0;
                            i++;
                            return true;
                        }
                    });

                    return Arrays.asList(files);
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<File> list) {
                mViewPager.setAdapter(new SamplePagerAdapter(list));
            }
        }.execute();
    }

    static class SamplePagerAdapter extends PagerAdapter {

        List<File> dataList;

        public SamplePagerAdapter(List<File> dataList) {
            this.dataList = dataList;
        }

        @Override
        public int getCount() {
            return null == dataList ? 0 : dataList.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            GLImageView photoView = new GLImageView(container.getContext());
            //photoView.setTileSource(new BitmapRegionTileSource(dataList.get(position).getAbsolutePath()));
            BitmapRegionTileSource.BitmapSource bitmapSource = null;
            bitmapSource = new BitmapRegionTileSource.FilePathBitmapSource(dataList.get(position).getAbsolutePath(), 1024);

            setViewTileSource(container.getContext(), photoView, bitmapSource, true, false, null);

            // Now just add PhotoView to ViewPager and return it
            container.addView(photoView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            TiledImageView mTextureView = (TiledImageView) object;
            mTextureView.destroy();
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public void setViewTileSource(
                final Context context, final GLImageView photoView, final BitmapRegionTileSource.BitmapSource bitmapSource, final boolean touchEnabled,
                final boolean moveToLeft, final Runnable postExecute) {
            //final View progressView = findViewById(R.id.loading);
            final AsyncTask<Void, Void, Void> loadBitmapTask = new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(Void... args) {
                    if (!isCancelled()) {
                        bitmapSource.loadInBackground();
                    }
                    return null;
                }

                protected void onPostExecute(Void arg) {
                    if (!isCancelled() && photoView != null) {
                        //progressView.setVisibility(View.INVISIBLE);
                        if (bitmapSource.getLoadingState() == BitmapRegionTileSource.BitmapSource.State.LOADED) {
                            photoView.setTileSource(
                                    new BitmapRegionTileSource(context, bitmapSource), null);
                            photoView.setTouchEnabled(touchEnabled);
                            if (moveToLeft) {
                                photoView.moveToLeft();
                            }
                        }
                    }
                    if (postExecute != null) {
                        postExecute.run();
                    }
                }
            };
            // We don't want to show the spinner every time we load an image, because that would be
            // annoying; instead, only start showing the spinner if loading the image has taken
            // longer than 1 sec (ie 1000 ms)
        /*progressView.postDelayed(new Runnable() {
            public void run() {
                if (loadBitmapTask.getStatus() != AsyncTask.Status.FINISHED) {
                    progressView.setVisibility(View.VISIBLE);
                }
            }
        }, 1000);*/
            loadBitmapTask.execute();
        }

    }

    private boolean isViewPagerActive() {
        return (mViewPager != null && mViewPager instanceof HackyViewPager);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (isViewPagerActive()) {
            outState.putBoolean(ISLOCKED_ARG, ((HackyViewPager) mViewPager).isLocked());
        }
        super.onSaveInstanceState(outState);
    }

}
